var lang = {
  "html": "100%",
  "css": "90%",
  "angular": "65%"
};

var multiply = 4;

$.each(lang, function (language, pourcent) {
  setTimeout(function () {
    $('#' + language + '-pourcent').html(pourcent);
  }, 600 * multiply);
  multiply++;

});


