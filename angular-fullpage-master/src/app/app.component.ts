import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  faHtml5,
  faCss3,
  faJs,
  faAngular,
  faNodeJs,
  faSass,
  faGulp,
  faNpm,
  faPhp,
  faLaravel,
  faJava,
  faBootstrap,
  faGit,
  faDocker,
  faGithub,
  faLinkedin
} from "@fortawesome/free-brands-svg-icons";
import {
  faCheck,
  faMobile,
  faDesktop,
  faServer,
} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./animate.scss"],
})
export class AppComponent {
  fullpage_api: any;
  colorFont: string;
  logoType: number;
  isAbout: boolean;
  config: any;
  faHtml5 = faHtml5;
  faCss3 = faCss3;
  faJs = faJs;
  faAngular = faAngular;
  faNodeJs = faNodeJs;
  faSass = faSass;
  faGulp = faGulp;
  faNpm = faNpm;
  faPhp = faPhp;
  faLaravel = faLaravel;
  faJava = faJava;
  faBootstrap = faBootstrap;
  faGit = faGit;
  faDocker = faDocker;
  faCheck = faCheck;
  faMobile = faMobile;
  faDesktop = faDesktop;
  faServer = faServer;
  faGithub = faGithub;
  faLinkedin = faLinkedin;

  constructor() {
    this.config = {
      licenseKey: "YOUR LICENSE KEY HERE",
      anchors: ["Home", "About", "Experience", "Work", "Contact"],
      navigation: true,
      autoScrolling:true,
      scrollHorizontally: true,

      afterResize: () => {
        console.log("After resize");
      },
      afterLoad: (origin: any, destination: any, direction: any) => {
         if (destination.anchor !== "Home") {
          this.colorFont = "#202123";
          this.logoType = 1;
        } else {
          this.colorFont = "#ffffff";
          this.logoType = 2;
        }
        if (destination.anchor === "About") {
          this.isAbout = true;
        } else {
          this.isAbout = false;
        }

      }
    };
  }

  ngOnInit() {
    this.colorFont = "#ffffff";
    this.logoType = 1
  }

  getRef(fullPageRef: any) {
    this.fullpage_api = fullPageRef;
  }
}
